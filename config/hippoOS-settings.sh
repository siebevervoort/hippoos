#!/bin/bash

## Hide Grub boot menu
echo "set timeout_style=hidden set timeout=0" | sudo tee /boot/grub/custom.cfg

## Turn of automatic updates


## Change ubuntu logo for Hippo dx logo


## Firewall rules
sudo ufw default allow outgoing
sudo ufw default deny incoming

# Allow ssh in firewall
sudo ufw allow ssh

## StartUp script
echo "# Execute the startup script startup.sh every boot" >> ~/.bashrc
echo "@reboot /startUp/startup.sh" >> ~/.bashrc


