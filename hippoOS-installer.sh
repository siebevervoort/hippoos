#!/bin/bash

function show_logo() {
  clear;
  echo " ---------------------------------------------------------------------------------------------"
  echo " ---------------------------------------------------------------------------------------------"
  echo "|                                                                                             |"
  echo "|                                                                                             |"
  echo "|   ▄█    █▄     ▄█     ▄███████▄    ▄███████▄  ▄██████▄       ████████▄  ▀████    ▐████▀     |"
  echo "|  ███    ███   ███    ███    ███   ███    ███ ███    ███      ███   ▀███   ███▌   ████▀      |"
  echo "|  ███    ███   ███▌   ███    ███   ███    ███ ███    ███      ███    ███    ███  ▐███        |"
  echo "| ▄███▄▄▄▄███▄▄ ███▌   ███    ███   ███    ███ ███    ███      ███    ███    ▀███▄███▀        |"
  echo "|▀▀███▀▀▀▀███▀  ███▌ ▀█████████▀  ▀█████████▀  ███    ███      ███    ███    ████▀██▄         |"
  echo "|  ███    ███   ███    ███          ███        ███    ███      ███    ███   ▐███  ▀███        |"
  echo "|  ███    ███   ███    ███          ███        ███    ███      ███   ▄███  ▄███     ███▄      |"
  echo "|  ███    █▀    █▀    ▄████▀       ▄████▀       ▀██████▀       ████████▀  ████       ███▄     |"
  echo "|                                                                                             |"
  echo "| S.P.A.T. Installer & Configuration script                                                   |"
  echo " ---------------------------------------------------------------------------------------------"
  echo " ---------------------------------------------------------------------------------------------"
}

# ______________________
## Global variables
# ______________________
matrix_vision_installed=0
postgresql_installed=0
java_installed=0
clean_up_complete=0
access_rights=0
xinit_installed=0

if [ $(uname -m) == 'aarch64' ]
then
    echo '❌ arm64 architecture is not supported'
    ARCH=arm64
    exit 1
elif [ $(uname -m) == 'x86_64' ]
then
    ARCH=amd64
else
    echo "❌ Architecture $(uname -m) not recognised"
    exit 1
fi

# ______________________
## Update
# ______________________
function update() {
    ## Install the latest updates
    echo "🔧 Updating Hippo OS"
    sudo apt-get update > /dev/null 2>&1
    sudo apt-get upgrade -y > /dev/null 2>&1
}

# ______________________
## Cleanup after install
# ______________________
function cleanup() {
  sudo apt purge cloud-init -y > /dev/null 2>&1
  sudo rm -rf /etc/cloud && sudo rm -rf /var/lib/cloud/ 1> /dev/null
  sudo apt autoremove -y > /dev/null 2>&1
  clean_up_complete=1
}

# ______________________
## Install functions
# ______________________
# Install Matrix vision SDK
function matrix_vision_sdk_install() {
  echo "⚙️ Installing Matrix vision SDK"️
  chmod u+x assets/Matrix-vision-SDK/install_mvGenTL_Acquire.sh
  cd assets/Matrix-vision-SDK
  sudo ./install_mvGenTL_Acquire.sh -u 1> /dev/null
  cd ../..

  echo "export GENICAM_ROOT=/opt/mvIMPACT_Acquire/runtime" >> ~/.bashrc
  echo "export GENICAM_LOG_CONFIG_V3_1=/opt/mvIMPACT_Acquire/runtime/log/config-unix/DefaultLogging.properties" >> ~/.bashrc
  echo "export GENICAM_ROOT_V3_1=/opt/mvIMPACT_Acquire/runtime" >> ~/.bashrc
  echo "export MVIMPACT_ACQUIRE_DIR=/opt/mvIMPACT_Acquire" >> ~/.bashrc
  echo "export MVIMPACT_ACQUIRE_DATA_DIR=/opt/mvIMPACT_Acquire/data" >> ~/.bashrc
  echo "export GENICAM_CACHE_V3_1=/opt/mvIMPACT_Acquire/runtime/cache/v3_1" >> ~/.bashrc
  echo "export GENICAM_GENTL64_PATH=/opt/mvIMPACT_Acquire/lib/x86_64" >> ~/.bashrc
  show_logo
}

# Install PostgreSQL
function postgresql_install() {
    echo "⚙️ Installing PostgreSQL"️
    ## Install
    sudo apt-get install postgresql -y 1> /dev/null

    ## Update configuration
    # Listen to local connections
    sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '0.0.0.0'/g" $(sudo -u postgres psql -t -P format=unaligned -c 'show config_file') 1> /dev/null

    # Trust all connections to DEV (postgres_hostbasedauthentification)
    echo "host HippoDB_DEV all 0.0.0.0/0 trust" | sudo -u postgres tee -a $(sudo -u postgres psql -t -P format=unaligned -c 'show hba_file') 1> /dev/null

    # Restart
    sudo systemctl restart postgresql.service 1> /dev/null

    ## Create databases
    sudo -u postgres createdb HippoDB_DEV 1> /dev/null
    sudo -u postgres createdb HippoDB 1> /dev/null

    ## Set pasword for postgres user
    sudo -u postgres psql -t -P format=unaligned -c "ALTER USER postgres WITH ENCRYPTED PASSWORD 'Hippo'" 1> /dev/null
}

# Install Java 17
function java_install() {
  echo "⚙️ Installing Java 17"️
  sudo apt install libfontconfig1 libxrender1 libxtst6 libxi6 -y > /dev/null 2>&1
  sudo dpkg -i assets/Java-17/bellsoft-jre17.0.2+9-linux-amd64-full.deb 1> /dev/null
}

function xinit_install() {
    sudo apt install xorg xinit xterm -y 1> /dev/null
}

# ______________________
# Install requirements
# ______________________
function install_requirements() {
  ## Matrix Vision SDK
  #matrix_vision_sdk_install
  #matrix_vision_installed=1

  ## PostgreSQL
  postgresql_install
  postgresql_installed=1

  ## JAVA
  java_install
  java_installed=1

  ## Xinit
  xinit_install
  xinit_installed=1
}

# ______________________
## Define access rights
# ______________________
function access_rights() {
   # Xinit permissions
   sudo chown hippo:hippo /etc/X11/Xwrapper.config
   echo "needs_root_rights=yes" >> /etc/X11/Xwraper.config
   access_rights=1
}

# ______________________
## Settings
# ______________________
## Hide Grub boot menu
#echo "set timeout_style=hidden set timeout=0" | sudo tee /boot/grub/custom.cfg

## Turn of automatic updates


## Change ubuntu logo for Hippo dx logo


## Firewall rules
#sudo ufw default allow outgoing
#sudo ufw default deny incoming

# Allow ssh in firewall
#sudo ufw allow ssh

## StartUp script
#echo "# Execute the startup script startup.sh every boot" >> ~/.bashrc
#echo "@reboot /startUp/startup.sh" >> ~/.bashrc

# ______________________
## Done
# ______________________
function check_done() {
  echo ""

  if [[ "$matrix_vision_installed" -eq 1 ]];
  then
    echo "✅ Matrix Vision SDK installed"
  fi

  if [[ "$postgresql_installed" -eq 1 ]];
  then
    echo "✅ PostgreSQL installed"
  fi

  if [[ "$java_installed" -eq 1 ]];
  then
    echo "✅ Java 17 installed"
  fi

  if [[ "$access_rights" -eq 1 ]];
  then
    echo "✅ Configure access rights"
  fi

  if [[ "$xinit_installed" -eq 1 ]];
  then
    echo "✅ Xinit server installed"
  fi

  if [[ "$clean_up_complete" -eq 1 ]];
  then
    echo "🚽 Cleanup complete!"
  fi

  echo ""
}

# ______________________
## Call functions
# ______________________
show_logo
update
install_requirements
cleanup
check_done