<img src="./assets/hippoOSLogo.png" title="" alt="" data-align="center">



### File structure

```
├── README.md
├── assets
│   ├── Java-17
│   │   └── bellsoft-jre17.0.2+9-linux-amd64-full.deb
│   ├── Matrix-vision-SDK
│   │   ├── install_mvGenTL_Acquire.sh
│   │   └── mvGenTL_Acquire-x86_64_ABI2-2.45.0.tgz
│   ├── application
│   │   └── dev-noHW-noFullscreen.jar
│   ├── hippoOSLogo.png
│   └── logoHIPPO.png
├── config
│   ├── embeded-application
│   │   ├── configure-access-rights.sh
│   │   ├── launch_spat_embedded.sh
│   │   └── start_spat_embedded.sh
│   ├── hippoOS-settings.sh
│   ├── hippoOS-users.sh
│   └── startup
│       └── startup.sh
├── hippoOS-installer.sh
└── install-scripts
    ├── install-openssh-server.sh
    ├── java-install.sh
    ├── matrix-vision-sdk-install.sh
    └── postgresql-install.sh
```
